#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <thread>
using namespace std;
//void Func(int n, int num)
//{
//	for (int i = 0; i < num; i++)
//	{
//		cout <<"线程："<< n << " " << i << endl;
//	}
//	cout << endl;
//	
//}
//int main()
//{
//	thread t1(Func, 1, 20);
//	thread t2(Func, 2, 30);
//	t1.join();
//	t2.join();
//	return 0;
//}

//int main()
//{
//	int n1, n2;
//	cin >> n1 >> n2;
//	thread t1([n1]( int num)
//		{
//			for (int i = 0; i < n1; i++)
//			{
//				cout << "线程：" << num << " " << i << endl;
//			}
//			cout << endl;
//		},1);
//	thread t2([n2](int num)
//		{
//			for (int i = 0; i < n2; i++)
//			{
//				cout << "线程：" << num << " " << i << endl;
//			}
//			cout << endl;
//		}, 2);
//	t1.join();
//	t2.join();
//
//	return 0;
//}
#include <vector>
//#include <mutex>
//int main()
//{
//	int m, n;
//	cin >> m >> n;
//	vector<int> arr;
//	mutex mtx;
//	arr.push_back(m);
//	arr.push_back(n);
//	
//	vector<thread> vthds(m);
//	for (int i = 0; i < arr[0]; i++)
//	{
//		
//		
//		vthds[i] = thread([i,arr,&mtx]()
//			{
//				mtx.lock();
//				for (int j = 0; j< arr[1]; j++)
//				{
//					cout << "线程:" << this_thread::get_id()
//						 << " " << "j:" << j << endl;
//					this_thread::sleep_for(chrono::milliseconds(200));
//				}
//				mtx.unlock();
//				cout << endl;
//			});
//		
//	}
//	for (auto& t : vthds)
//	{
//		t.join();
//	}
//	return 0;
//}
//#include<mutex>
//
//int x = 0;
//mutex mtx;

//void Func(int n)
//{
//	//// 并行
//	//for (int i = 0; i < n; i++)
//	//{
//	//	mtx.lock();
//	//	++x;
//	//	mtx.unlock();
//	//}
//
//	// 快
//	// 串行
//	mtx.lock();
//	for (int i = 0; i < n; i++)
//	{
//		++x;
//	}
//	mtx.unlock();
//}
//int main()
//{
//	int n = 100000;
//	size_t begin = clock();
//
//	thread t1(Func, n);
//	thread t2(Func, n);
//
//	t1.join();
//	t2.join();
//	size_t end = clock();
//
//	cout << x << endl;
//	cout << end - begin << endl;
//
//	return 0;
//}
 #include<mutex>
//
//int main()
//{
//	int n = 100000;
//	int x = 0;
//	mutex mtx;
//	size_t begin = clock();
//
//	thread t1([&,n](){
//			mtx.lock();
//			for (int i = 0; i < n; i++)
//			{
//				/*cout << &x << endl;
//				cout << &n << endl;*/
//				++x;
//			}
//			mtx.unlock();
//		});
//
//	thread t2([&,n]() {
//			for (int i = 0; i < n; i++)
//			{
//				/*cout << &x << endl;
//				cout << &n << endl;*/
//				mtx.lock();
//				++x;
//				mtx.unlock();
//			
//			}
//		});
//
//	t1.join();
//	t2.join();
//	size_t end = clock();
//
//	cout << x << endl;
//	cout << end - begin << endl;
//
//	return 0;
//}

//
//int main()
//{
//	int n = 100000;
//	int x = 0;
//	mutex mtx;
//	thread t1([&, n]() {
//		try {
//			lock_guard<mutex> lck(mtx);
//			for (int i = 0; i < n; i++)
//			{
//
//				if (i % 2 == 0)
//					throw exception("异常");
//				++x;
//
//			}
//
//		}
//		catch (const exception& e)
//		{
//			cout << e.what() << endl;
//		}
//		});
//
//
//	thread t2([&, n]() {
//		for (int i = 0; i < n; i++)
//		{
//			lock_guard<mutex> lck(mtx);
//			++x;
//		}
//		});
//
//	t1.join();
//	t2.join();
//	cout << x << endl;
//	return 0;
//}

//#include<mutex>
//
//int x = 0;
//recursive_mutex mtx;
//
//void Func(int n)
//{
//	if (n == 0)
//		return;
//
//	mtx.lock();
//	++x;
//	mtx.unlock();
//	Func(n - 1);
//	
//}
//
//int main()
//{
//	thread t1(Func, 1000);
//	thread t2(Func, 2000);
//
//	t1.join();
//	t2.join();
//
//	cout << x << endl;
//
//	return 0;
//}
//class Rate
//{
//public:
//	Rate(double rate) : _rate(rate)
//	{}
//
//	double operator()(double money, int year)
//	{
//		return money * _rate * year;
//	}
//
//private:
//	double _rate;
//};
//int main()
//{
//	int x = 0, y = 1;
//	int m = 0, n = 1;
//
//	auto swap1 = [x,y](int& rx, int& ry)
//	{
//		int tmp = rx;
//		rx = ry;
//		ry = tmp;
//	};
//	cout << sizeof(swap1) << endl;
//
//		// 函数对象
//	double rate = 0.49;
//	Rate r1(rate);
//	r1(10000, 2);
//
//	// lambda
//	auto r2 = [=](double monty, int year)->double {return monty * rate * year; };
//	r2(10000, 2);
//
//	auto f1 = [] {cout << "hello world" << endl; };
//	auto f2 = [] {cout << "hello world" << endl; };
//
//	f1();
//	f2();
//
//	return 0;
//}

//#include <iostream>
//#include <thread>
//
//void thread_function() {
//    std::cout << "线程正在执行工作..." << std::endl;
//    // 模拟一些工作负载
//    std::this_thread::sleep_for(std::chrono::seconds(2));
//    std::cout << "线程工作完成。" << std::endl;
//}
//
//int main() {
//    std::thread t(thread_function);
//
//    // 分离线程
//    t.detach();
//
//    // 主线程继续执行，不会等待分离的线程结束
//    std::cout << "主线程继续执行，不会等待线程 " << t.get_id() << " 结束。" << std::endl;
//
//    // 由于线程已经被分离，这里调用 join() 将无效果
//     t.join(); // 这行代码将导致未定义行为
//
//    return 0;
//}
//#include <thread>
//#include <condition_variable>
//#include <mutex>
//int x = 1;
//int n = 100;
//mutex mtx;
//condition_variable cv;
//void fucn1(int n)
//{
//
//	while(true)
//	{
//		unique_lock<mutex> lck(mtx);
//		if (x >= 100)
//			break;
//		if (x % 2 == 0) //是偶数就阻塞等待
//			cv.wait(lck);
//		cout << "t1:" << this_thread::get_id() << ":" << x << endl;
//		++x;
//		cv.notify_one();
//	}
//}
//void fucn2(int n)
//{
//	
//	while(true)
//	{
//		unique_lock<mutex> lck(mtx);
//		if (x >= 100)
//			break;
//		if (x % 2 != 0) //是奇数就阻塞等待
//			cv.wait(lck);
//		cout << "t2:" << this_thread::get_id() << ":" << x << endl;
//		++x;
//		cv.notify_one();
//	}
//
//}
//int main()
//{
//	thread t1(fucn1, n);
//	thread t2(fucn2, n);
//	t1.join();
//	t2.join();
//	return 0;
//}
//int main()
//{
//	mutex mtx;
//	condition_variable cv;
//
//	int n = 100;
//	int x = 0;
//
//	// 问题1：如何保证t1先运行，t2阻塞？
//	// 问题2：如何防止一个线程不断运行？
//	thread t1([&, n]() {
//		while (1)
//		{
//			unique_lock<mutex> lock(mtx);
//			if (x >= 100)
//				break;
//
//			//if (x % 2 == 0) // 偶数就阻塞
//			//{
//			//	cv.wait(lock);
//			//}
//			cv.wait(lock, [&x]() {return x % 2 != 0; });
//
//			cout << this_thread::get_id() << ":" << x << endl;
//			++x;
//
//			cv.notify_one();
//		}
//		});
//
//	thread t2([&, n]() {
//		while (1)
//		{
//			unique_lock<mutex> lock(mtx);
//			if (x > 100)
//				break;
//
//			//if (x % 2 != 0) // 奇数就阻塞
//			//{
//			//	cv.wait(lock);
//			//}
//			cv.wait(lock, [&x](){return x % 2 == 0; });
//
//
//			cout << this_thread::get_id() << ":" << x << endl;
//			++x;
//
//			cv.notify_one();
//		}
//		});
//
//	t1.join();
//	t2.join();
//
//	return 0;
//}
//#include <iostream>
//#include <atomic>
//#include <thread>
//#include <vector>
//
//std::atomic<int> counter(0); // 线程安全的原子计数器
//
//int main() {
//    const int num_threads = 4;
//    std::vector<std::thread> threads;
//
//    // 使用 lambda 表达式创建并启动线程
//    for (int i = 0; i < num_threads; ++i) {
//        threads.push_back(std::thread([=]() {
//            int n = 100; // 每个线程递增的次数
//            for (int j = 0; j < n; ++j) {
//                // 使用 fetch_add 原子地递增 counter
//                counter.fetch_add(1, std::memory_order_relaxed);
//            }
//            }));
//    }
//
//    // 等待所有线程完成
//    for (auto& th : threads) {
//        if (th.joinable()) {
//            th.join();
//        }
//    }
//
//    // 输出最终的计数器值
//    //std::cout << "Final counter value: " << counter << std::endl;
//    printf("Final counter value: %d", counter.load());
//    return 0;
//}
#include<map>
#include<functional>
//int f(int a, int b)
//{
//	cout << "int f(int a, int b)" << endl;
//	return a + b;
//}
//
//struct Functor
//{
//public:
//	int operator() (int a, int b)
//	{
//		cout << "int operator() (int a, int b)" << endl;
//
//		return a + b;
//	}
//};
//
//class Plus
//{
//public:
//	Plus(int rate = 2)
//		:_rate(rate)
//	{}
//
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//
//	double plusd(double a, double b)
//	{
//		return (a + b) * _rate;
//	}
//
//private:
//	int _rate = 2;
//};
//int main()
//{
//	function<int(int, int)> f1 = &Plus::plusi;
//	function<int(int, int)> f1 = Plus::plusi;
//
//	function<double(Plus*, double, double)> f2 = &Plus::plusd;
//
//	cout << f1(1, 2) << endl;
//	//cout << f2(&Plus(), 20, 20) << endl;
//
//	Plus pl(3);
//	cout << f2(&pl, 20, 20) << endl;
//
//	function<double(Plus, double, double)> f2 = &Plus::plusd;
//
//	cout << f1(1, 2) << endl;
//	cout << f2(Plus(), 20, 20) << endl;
//
//	Plus pl(3);
//	cout << f2(pl, 20, 20) << endl;
//
//	return 0;
//}
//int main()
//{
//	//int(*pf1)(int,int) = f;
//	//map<string, >
//
//	/*function<int(int, int)> f1 = f;
//	function<int(int, int)> f2 = Functor();
//	function<int(int, int)> f3 = [](int a, int b) {
//		cout << "[](int a, int b) {return a + b;}" << endl;
//		return a + b;
//	};
//
//	cout << f1(1, 2) << endl;
//	cout << f2(10, 20) << endl;
//	cout << f3(100, 200) << endl;*/
//
//	map<string, function<int(int, int)>> opFuncMap;
//	opFuncMap["函数指针"] = f;
//	opFuncMap["仿函数"] = Functor();
//	opFuncMap["lambda"] = [](int a, int b) {
//		cout << "[](int a, int b) {return a + b;}" << endl;
//		return a + b;
//	};
//	cout << opFuncMap["函数指针"](1, 2) << endl;
//	cout<< opFuncMap["仿函数"](1, 2) << endl;
//	cout << opFuncMap["lambda"](1, 2) << endl;
//
//
//
//	return 0;
//}
//
//#include <iostream>
//#include <functional>
//
//
//void print(int a, int b) {
//    std::cout << a << " and " << b << std::endl;
//}
//
//int main() {
//    print(10, 5);
//    // 创建一个 std::function 对象，绑定 print_sum 函数
//    // 但是交换了参数的顺序
//    auto swapped_function = std::bind(print, std::placeholders::_1, std::placeholders::_2);
//
//    // 调用交换参数后的函数
//    swapped_function(5, 10); // 输出 "The sum of 10 and 5 is 15"
//
//    return 0;
//}

#include <iostream>
#include <functional>
#include <map>
#include <string>

// 一个示例类
class Calculator {
public:
    // 类成员函数，接受两个参数
    int add(int a, int b) {
        return a + b;
    }
};

// 一个自由函数，接受两个参数
int multiply(int a, int b) {
    return a * b;
}

int main() {
    //用bind直接绑死对象。
    std::function<int(int, int)> add1 = std::bind(&Calculator::add, Calculator(), std::placeholders::_1, std::placeholders::_2);
    std::map<std::string, std::function<int(int, int)>> funcMap =
    {
      {"*",multiply},
        {"+",add1 }
    };
    //std::cout << funcMap["*"](1, 2) << std::endl;
    //std::cout << funcMap["+"](10, 30) << std::endl;
    for (auto& e : funcMap)
    {
        std::cout << "[" << e.first <<"]: " << e.second(10, 20) << std::endl;
    }
    return 0;
}