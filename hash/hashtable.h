#pragma once
#include <vector>
enum  State
{
	EMPTY, //空
	EXIST, //存在
	DELETE //删除
};

template <class	K, class V>
struct hashData
{
	pair<K, V> _kv;
	State _state = EMPTY;
};

template <class K, class V>
class hashTable
{
	typedef  hashData<K, V> Node;
public:
	bool Insert(const pair<K, V>& kv)
	{
		//当负载因子为大于0.7时就扩容
		if (_tables.size() == 0 || _n * 10 / _tables.size() >= 7)
		{
			if (Find(kv.first))
				return false;
			size_t newsize = _tables.size() == 0 ? 10 : _tables.size() * 2;
			hashTable <K, V> newht;
			newht._tables.resize(newsize);
			//遍历旧表，重新映射到新表
			for (auto& data : _tables)
			{
				if (data._state == EXIST)
				{
					newht.Insert(data._kv);
				}
			}
			_tables.swap(newht._tables);
		}
		size_t hashi = kv.first % _tables.size();
		//线性探测
		size_t i = 1;
		size_t index = hashi;
		while (_tables[index]._state == EXIST)
		{
			index = hashi + i;
			index %= _tables.size();
			++i;
		}
		_tables[index]._kv = kv;
		_tables[index]._state = EXIST;
		_n++;
		return true;
	}
	Node* Find(const K& key)
	{
		if (_tables.size() == 0)
		{
			return nullptr;
		}
		size_t hashi = key % _tables.size();
		size_t i = 1;
		size_t index = hashi;
		while (_tables[index]._state != EMPTY)
		{
			if (_tables[index]._state == EXIST
				&& _tables[index]._kv.first == key)
			{
				return &_tables[index];
			}
			index = hashi + i;
			index %= _tables.size();
			++i;
			if (hashi == index)
			{
				break;
			}

		}
		return nullptr;

	}
	bool Erase(const K& key)
	{
		Node* ret = Find(key);
		if (ret)
		{
			ret->_state = DELETE;
			--_n;
			return true;
		}
		else
			return false;
	}
private:
	vector<Node> _tables;
	size_t _n;//记录元素个数
};
void TestHashTable1()
{
	int a[] = { 3, 33, 2, 13, 5, 12, 1002 };
	hashTable<int, int> ht;
	for (auto e : a)
	{
		ht.Insert(make_pair(e, e));
	}

	ht.Insert(make_pair(15, 15));

	if (ht.Find(13))
	{
		cout << "13在" << endl;
	}
	else
	{
		cout << "13不在" << endl;
	}

	ht.Erase(13);

	if (ht.Find(13))
	{
		cout << "13在" << endl;
	}
	else
	{
		cout << "13不在" << endl;
	}
}

