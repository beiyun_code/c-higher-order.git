#pragma once
//定义二叉树节点
namespace zbl
{
	template<class K>
	struct BSTreeNode
	{
		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;
		K _key;
		

		BSTreeNode(const K& key)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
			
		{}

	};
	template<class K>
	class BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		BSTree() = default; // 制定强制生成默认构造
		BSTree(const BSTree<K>& k)
		{
			_root = Copy(k._root);
		}
		//现代拷贝写法
		BSTree<K>& operator=(BSTree<K> t)
		{
			swap(_root, t._root);
			return *this;
		}

		~BSTree()
		{
			Destroy(_root);
		}

		//递归
		bool FindR(const K& key)
		{
			return _FindR(_root, key);

		}

		bool InsertR(const K& key)
		{
			return _InsertR(_root, key);
		}

		bool EraseR(const K& key)
		{
			return _EraseR(_root, key);
		}



		void Inorder()
		{
			_InOrder(_root);
			cout << endl;
		}
	protected:
		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}
		bool _EraseR(Node*& root, const K& key)
		{
			if (root == nullptr)
				return false;
			//小于根就左边找
			if (root->_key > key)
			{
				return _EraseR(root->_left, key);
			}
			//大于根就右边找
			else if (root->_key < key)
			{
				return _EraseR(root->_right, key);
			}
			//相等
			else
			{	//删除

				Node* del = root; //先记录要删除的节点，
				//右为空直接删除			
				if (root->_right == nullptr)
					root = root->_left;
				//左为空直接删除
				else if (root->_left == nullptr)
					root = root->_right;

				//两边都不为空，以右子树为例 找最小的            
				else
				{
					Node* MinRight = root->_right;
					while (MinRight->right)
					{
						MinRight = MinRight->left; //找到右边最小的节点
					}
					swap(root->_key, MinRight->_key);//替换法：右边节点最小的值和根交换值
					return _EraseR(root->_right, key);//替换了之后我们要删除就是它，
					//我们再次递归是不是就变成了上面两种情况，左右各为空的情况。
				}
				delete del;
				return true;
			}
		}
		bool _InsertR(Node*& root, const K& key)
		{
			if (root == nullptr)
				root = new Node(key);

			if (root->_key < key)
			{
				return _InsertR(root->_right, key);
			}
			else if (root->_key > key)
			{
				return _InsertR(root->_left, key);
			}
			else
			{
				return false;
			}


		}
		bool _FindR(Node* root, const K& key)
		{
			if (root == nullptr)
				return false;
			if (root->_key == key)
				return true;
			else if (root->_key < key)
				return _FindR(root->_right, key);
			else
				return _FindR(root->_left, key);
		}
		bool Erase(const K& key)
		{
			if (_root == nullptr)
				return false; //树为空直接返回
			//遍历找到要删除的数
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else  //删除
				{
					//左为空
					if (cur->_left == nullptr)
					{
						if (cur == _root) //如果左子树全部为空，这时如果删除根节点，根节点是没有父亲节点。下面判断就是空指针。
						{
							_root = cur->_right;
						}
						else
						{
							if (parent->_left == cur)
							{
								parent->_left = cur->_right;
							}
							else
							{
								parent->_right = cur->_right;
							}
						}
						delete cur;
					}
					//右为空
					else if (cur->_right == nullptr)
					{
						if (cur == _root) //同理左子树
						{
							_root = cur->_left;
						}
						else
						{
							if (parent->_left == cur)
							{
								parent->_left = cur->_left;
							}
							else
							{
								parent->_right = cur->_left;
							}
						}
						delete cur;
					}
					//两边都不为空 选择右子树小的节点或者左子树最大的节点。
					//以左子树最大节点为例
					else
					{
						Node* pmaxLeft = cur; //如果最大左子树的父亲节点设置为空，下面循环没有进去，父亲没有更新，下面条件判断就会出现空指针问题。
						Node* maxLeft = cur->_left;
						while (maxLeft->_right)
						{
							pmaxLeft = maxLeft;
							maxLeft = maxLeft->_right;
						}
						cur->_key = maxLeft->_key;
						if (pmaxLeft->_right == maxLeft) //这判断也是一样 如果上面循环没有进去 那么 8这个节点右子树就打乱结构了。
						{
							pmaxLeft->_right = maxLeft->_left;
						}
						else
						{
							pmaxLeft->_left = maxLeft->_left;
						}

						delete maxLeft;

					}
					return true;
				}
			}
			return false;
		}
		bool Find(const K& key)
		{
			if (_root == nullptr) //如果二叉树为空直接返回假
			{
				return false;
			}
			Node* cur = _root;
			while (cur)//遍历左右子树
			{
				if (cur->_key < key) // 根小于要查找数右子树找
				{
					cur = cur->_right;
				}
				else if (cur->_key > key) // 根大于要查找数左子树找
				{
					cur = cur->_left;
				}
				else    //等于就是找到了 返回真
				{
					return true;
				}
			}
			return false; //循环结束没找到返回假
		}
		bool Insert(const K& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}

			}
			cur = new Node(key);
			//链接
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			return true;
		}
		Node* Copy(Node* root)
		{
			if (root == nullptr)
				return nullptr;

			Node* newRoot = new Node(root->_key);
			newRoot = Copy(root->_left);
			newRoot = Copy(root->_right);
			return newRoot;
		}
		void Destroy(Node*& root)
		{
			if (root == nullptr)
				return;
			Destroy(root->_left);
			Destroy(root->_right);
			delete root;
			root = nullptr;
		}
	private:
		Node* _root = nullptr;

	};
}
namespace gx
{
	template<class K, class V>
	struct BSTreeNode
	{
		BSTreeNode<K, V>* _left;
		BSTreeNode<K, V>* _right;
		K _key;
		V _value;

		BSTreeNode(const K& key, const V& value)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
			, _value(value)
		{}

	};
	template<class K, class V>
	class BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		
		
		bool Erase(const K& key)
		{
			if (_root == nullptr)
				return false; //树为空直接返回
			//遍历找到要删除的数
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else  //删除
				{
					//左为空
					if (cur->_left == nullptr)
					{
						if (cur == _root) //如果左子树全部为空，这时如果删除根节点，根节点是没有父亲节点。下面判断就是空指针。
						{
							_root = cur->_right;
						}
						else
						{
							if (parent->_left == cur)
							{
								parent->_left = cur->_right;
							}
							else
							{
								parent->_right = cur->_right;
							}
						}
						delete cur;
					}
					//右为空
					else if (cur->_right == nullptr)
					{
						if (cur == _root) //同理左子树
						{
							_root = cur->_left;
						}
						else
						{
							if (parent->_left == cur)
							{
								parent->_left = cur->_left;
							}
							else
							{
								parent->_right = cur->_left;
							}
						}
						delete cur;
					}
					//两边都不为空 选择右子树小的节点或者左子树最大的节点。
					//以左子树最大节点为例
					else
					{
						Node* pmaxLeft = cur; //如果最大左子树的父亲节点设置为空，下面循环没有进去，父亲没有更新，下面条件判断就会出现空指针问题。
						Node* maxLeft = cur->_left;
						while (maxLeft->_right)
						{
							pmaxLeft = maxLeft;
							maxLeft = maxLeft->_right;
						}
						cur->_key = maxLeft->_key;
						if (pmaxLeft->_right == maxLeft) //这判断也是一样 如果上面循环没有进去 那么 8这个节点右子树就打乱结构了。
						{
							pmaxLeft->_right = maxLeft->_left;
						}
						else
						{
							pmaxLeft->_left = maxLeft->_left;
						}

						delete maxLeft;

					}
					return true;
				}
			}
			return false;
		}
		Node* Find(const K& key)
		{
			if (_root == nullptr) //如果二叉树为空直接返回假
			{
				return nullptr;
			}
			Node* cur = _root;
			while (cur)//遍历左右子树
			{
				if (cur->_key < key) // 根小于要查找数右子树找
				{
					cur = cur->_right;
				}
				else if (cur->_key > key) // 根大于要查找数左子树找
				{
					cur = cur->_left;
				}
				else    //等于就是找到了 返回真
				{
					return cur;
				}
			}
			return nullptr; //循环结束没找到返回假
		}
		bool Insert(const K& key, const V& value)
		{
			if (_root == nullptr)
			{
				_root = new Node(key,value);
				return true;
			}
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}

			}
			cur = new Node(key,value);
			//链接
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			return true;
		}
		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}
		protected:
			void _InOrder(Node* root)
			{
				if (root == nullptr)
					return;

				_InOrder(root->_left);
				cout << root->_key << ":" << root->_value << endl;
				_InOrder(root->_right);
			}
	private:
		Node* _root = nullptr;

	};
}