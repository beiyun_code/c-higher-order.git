﻿#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
using namespace std;
//
//struct Point
//{
//	int _x;
//	int _y;
//};
//int main()
//{
//	int x1 = 1;
//	int x2 = { 2 };
//	// int x4(1);
//
//	// 可以省略赋值符号
//	int x3{ 3 };
//	int array1[]{ 1, 2, 3, 4, 5 };
//	int array2[5]{ 0 };
//	Point p{ 1, 2 };
//	// C++11中列表初始化也可以适用于new表达式中
//	int* pa = new int[4]{ 0 };
//
//	return 0;
//}
//class Date
//{
//public:
//	//explicit Date(int year, int month, int day)
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1(2022, 1, 1); // old style
//
//	// C++11支持的列表初始化，这里会调用构造函数初始化
//	Date d2 = { 2024, 5, 20 };
//	Date d3 { 2024, 5, 21 };
//
//	return 0;
//}
//int main()
//{
//	vector<int> v1 = { 1,2,3,4,5 };
//	vector<int> v2 = { 10,20,30 };
//	vector<int> v3 = { 10,20,30,1,1,2,2,2,2,2,2,1,1,1,1,1,1,1,1,2,1,1,2 };
//
//	list<int> lt1 = { 1,2,3,4,5 };
//	list<int> lt2 = { 10,20,30 };
//
//	auto i1 = { 10,20,30,1,1,2,2,2,2,2,2,1,1,1,1,1,1,1,1,2,1,1,2 };
//	auto i2 = { 10,20,30 };
//	cout << typeid(i1).name() << endl;
//	cout << typeid(i2).name() << endl;
//
//	initializer_list<int>::iterator it1 = i1.begin();
//	initializer_list<int>::iterator it2 = i2.begin();
//	cout << it1 << endl;
//	cout << it2 << endl;
//	 //*it1 = 1;
//
//	initializer_list<int> i3 = { 10,20,30 };
//	initializer_list<int>::iterator it3 = i3.begin();
//	cout << it3 << endl;
//
//	Date d1(2023,5,20);
//	Date d2(2023,5,21);
//	// initializer_list<Date>
//	vector<Date> vd1 = {d1, d2};
//	vector<Date> vd2 = { Date(2023,5,20), Date(2023,5,21) };
//	vector<Date> vd3 = { {2023,5,20}, {2023,5,20} };
//
//	map<string, string> dict = { {"sort", "排序"},{"string", "字符串"},{"Date", "日期"} };
//	pair<string, string> kv1 = { "Date", "日期" };
//	pair<string, string> kv2 { "Date", "日期" };
//
//	return 0;
//}
//	int main()
//{
//	const int x = 1;
//	double y = 2.2;
//
//	cout << typeid(x * y).name() << endl;
//
//	decltype(x * y) ret; // ret的类型是double
//	decltype(&x) p;      // p的类型是const int*
//	cout << typeid(ret).name() << endl;
//	cout << typeid(ret).name() << endl;
//	cout << typeid(p).name() << endl;
//
//	// vector存储的类型跟x*y表达式返回值类型一致
//	// decltype推导表达式类型，用这个类型实例化模板参数或者定义对象
//	vector<decltype(x* y)> v;
//
//	return 0;
//}
//void func(const int& val)
//{
//	cout << "func(const int& val)" << endl;
//}
//void func(const int&& val)
//{
//	cout << "func(const int&& val)" << endl;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	func(a);
//	func(a + b);
//	return 0;
//}
#include "String.h"
//int main()
//{
//	/*gx::string s1("hello world");
//	gx::string s2(s1);
//	gx::string s3 = (s2 + '!');*/
//
//	//gx::string ret1 = gx::to_string(1234);
//	gx::string ret1;
//	ret1 = gx::to_string(1234);
//	return 0;
//}
////////////////////////////////////////////////////////////////////////
//int main()
//{
//	list<gx::string> lt;
//	/*gx::string s1("你好世界");
//	lt.push_back(s1);
//	lt.push_back(move(s1));*/
//	lt.push_back("你好世界");
//	lt.push_back(gx::string("你好世界"));
//	return 0;
//}
//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//template<typename T>
//void PerfectForward(T&& t)
//{
//	Fun(forward<T>(t));
//}
//int main()
//{
//	PerfectForward(10);// 右值
//	int a;
//	PerfectForward(a);// 左值
//	PerfectForward(std::move(a)); // 右值
//	const int b = 8;
//	PerfectForward(b);// const 左值
//	PerfectForward(std::move(b)); // const 右值
//	return 0;
//}
template<class T>
struct ListNode
{
	ListNode* _next = nullptr;
	ListNode* _prev = nullptr;
	T _data;
};
template<class T>
class List
{
	typedef ListNode<T> Node;
public:
	List()
	{
		_head = new Node;
		_head->_next = _head;
		_head->_prev = _head;
	}
	void PushBack(T&& x)
	{
		//Insert(_head, x);
		Insert(_head, std::forward<T>(x));
	}
	void PushFront(T&& x)
	{
		//Insert(_head->_next, x);
		Insert(_head->_next, std::forward<T>(x));
	}
	void Insert(Node* pos, T&& x)
	{
		Node* prev = pos->_prev;
		Node* newnode = new Node;
		newnode->_data = x; // 关键位置
		// prev newnode pos
		prev->_next = newnode;
		newnode->_prev = prev;
		newnode->_next = pos;
		pos->_prev = newnode;
	}
	void Insert(Node* pos, const T& x)
	{
		Node* prev = pos->_prev;
		Node* newnode = new Node;
		newnode->_data = x; // 关键位置
		// prev newnode pos
		prev->_next = newnode;
		newnode->_prev = prev;
		newnode->_next = pos;
		pos->_prev = newnode;
	}
private:
	Node* _head;
};

int main()
{
	List<gx::string> lt;
	lt.PushBack("1111");
	lt.PushFront("2222");
	return 0;
}